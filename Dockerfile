FROM microsoft/aspnetcore:2.0
ENV ASPNETCORE_URLS=http://+:80
COPY out .
ENTRYPOINT ["dotnet", "MyApi.dll"]
